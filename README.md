# CI/CD component for the IGWN Software Archive

This project provides [CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
to upload files to the
[IGWN Software Archive](https://computing.docs.ligo.org/guide/software/distribution).
